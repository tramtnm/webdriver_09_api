package selenium;

import org.testng.annotations.Test;



import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class Topic_06_WebBrowser {
	
	WebDriver driver;
	By email = By.xpath("//input[@id='mail']");
	By age_18 = By.xpath("//input[@id='under_18']");
	By education = By.xpath("//textarea[@id='edu']");
	By role_1 = By.xpath("//select[@id='job1']");
	By develop = By.xpath("//input[@id='development']");
	By slide_1 = By.xpath("//input[@id='slider-1']");
	By bt_enable = By.xpath("//button[@id='button-enabled']");
	
	By pass = By.xpath("//input[@id='password']");
	By age_disable = By.xpath("//input[@id='radio-disabled']");
	By bio = By.xpath("//textarea[@id='bio']");
	By role_2 = By.xpath("//select[@id='job2']");
	By interest_chkdis = By.xpath("//input[@id='check-disbaled']");
	By slide_2 = By.xpath("//input[@id='slider-2']");
	By bt_disable = By.xpath("//button[@id='button-disabled']");		
  
  
  @BeforeClass
  public void beforeClass() {
	  driver = new FirefoxDriver();
	  //driver = new ChromeDriver();
	  driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
	  driver.manage().window().maximize();
  }

  @Test
  public void TC_1_CheckElementDisplayed() throws Exception {
	  driver.get("https://daominhdam.github.io/basic-form/index.html");
	  // Verify  Email, Age (Under 18), Education displayed
	  
	  WebElement email = driver.findElement(By.xpath("//input[@id='mail']"));
	  Assert.assertTrue(email.isDisplayed());
	  email.sendKeys("Automation Test");
	  
	  WebElement age_u18 = driver.findElement(By.xpath("//input[@id='under_18']"));
	  Assert.assertTrue(age_u18.isDisplayed());
	  age_u18.click();
	  
	  WebElement education = driver.findElement(By.xpath("//textarea[@id='edu']"));
	  Assert.assertTrue(education.isDisplayed());
	  education.sendKeys("Automation Test");
	  
	  Thread.sleep(2000);
	  
  }
  
  @Test
  public void TC_2_CheckElementEnable() throws Exception {
	  driver.get("https://daominhdam.github.io/basic-form/index.html");
	  // Verify  Email, Age (Under 18), Education displayed
	 
	  isElementEnable(email);
	  isElementEnable(age_18);
	  isElementEnable(education);
	  isElementEnable(role_1);
	  isElementEnable(develop);
	  isElementEnable(slide_1);
	  isElementEnable(bt_enable);
	  
	  isElementEnable(pass);
	  isElementEnable(age_disable);
	  isElementEnable(bio);
	  isElementEnable(role_2);
	  isElementEnable(interest_chkdis);
	  isElementEnable(slide_2);
	  isElementEnable(bt_disable);
	   
  }
  
  
  @Test
  public void TC_3_CheckElementSelected() throws Exception {
	  driver.get("https://daominhdam.github.io/basic-form/index.html");
	  driver.findElement(age_18).click();
	  driver.findElement(develop).click();
	  Thread.sleep(2000);
	  
	  Assert.assertTrue(driver.findElement(age_18).isSelected());
	  Assert.assertTrue(driver.findElement(develop).isSelected());
	  driver.findElement(develop).click();
	  Assert.assertFalse(driver.findElement(develop).isSelected());
	  
	  
  }
  
  
  public void isElementEnable(By by) {
	  WebElement element = driver.findElement(by);
	  if(element.isEnabled()) {
		  System.out.println("Element is enable: ");
	  }else {
		  System.out.println("Element is disable: ");
	  }
  }
  
  
  
  
  @AfterClass
  public void afterClass() {
	  driver.quit();
  }

}

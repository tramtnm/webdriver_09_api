package selenium;

import org.testng.annotations.Test;



import org.testng.annotations.BeforeClass;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class Topic_07_Textbox_Textarea {
	
	WebDriver driver;
	String input_user = "mngr197733";
	String input_pass = "ujUjAgE";
	
	//data input
	String customer_name = "Mandy Wong";
	String dob = "1982-12-22";
	String add = "123 Nathan";
	String city = "HongKong";
	String state = "KowLoo";
	String pin = "715467";
	String mobile = "0934567856";
	String email = "mandywong" + randomData() + "@gmail.com";
	String pass = "Th@nhha1204";
 
	//Add Customer
	By customername_txt = By.xpath("//input[@name='name']");
	By male_rb = By.xpath("//input[@value='m']");
	By female_rb = By.xpath("//input[@value='f']");
	By dob_txt = By.xpath("//input[@id='dob']");
	By add_txt = By.xpath("//textarea[@name='addr']");
	By city_txt = By.xpath("//input[@name='city']");
	By state_txt = By.xpath("//input[@name='state']");
	By pino_txt = By.xpath("//input[@name='pinno']");
	By mobile_txt = By.xpath("//input[@name='telephoneno']");
	By email_txt = By.xpath("//input[@name='emailid']");
	By pass_txt = By.xpath("//input[@name='password']");
	By submit_bt = By.xpath("//input[@name='sub']");
	
	//Verify data
	By customerId = By.xpath("//td[text()='Customer ID']/following-sibling::td");
	By cusname_verify = By.xpath("//td[text()='Customer Name']/following-sibling::td");
	By gender_verify = By.xpath("//td[text()='Gender']/following-sibling::td");
	By dob_verify = By.xpath("//td[text()='Birthdate']/following-sibling::td");
	By add_verify = By.xpath("//td[text()='Address']/following-sibling::td");
	By city_verify = By.xpath("//td[text()='City']/following-sibling::td");
	By state_verify = By.xpath("//td[text()='State']/following-sibling::td");
	By pin_verify = By.xpath("//td[text()='Pin']/following-sibling::td");
	By mobile_verify = By.xpath("//td[text()='Mobile No.']/following-sibling::td");
	By email_verify = By.xpath("//td[text()='Email']/following-sibling::td");
	
	//Edit data
	By cusid_edit = By.xpath("//input[@name='cusid']");
	By sub_edit = By.xpath("//input[@name='AccSubmit']");
	
	
	
  
  
  @BeforeClass
  public void beforeClass() {
	  driver = new FirefoxDriver();
	  //driver = new ChromeDriver();
	  driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
	  driver.manage().window().maximize();
  }

  @Test
  public void TC_1_Textbox_Textarea() {
	  driver.get("http://demo.guru99.com/v4");
	  
	  //Log in 
	  WebElement login_id = driver.findElement(By.xpath("//input[@name='uid']"));
	  WebElement login_pass = driver.findElement(By.xpath("//input[@name='password']"));
	  WebElement login_bt = driver.findElement(By.xpath("//input[@name='btnLogin']"));
	  login_id.sendKeys(input_user);
	  login_pass.sendKeys(input_pass);
	  login_bt.click();
	  
	  //verify Guru99 Bank Manager HomePage displayed
	 // Assert.assertEquals(driver.getTitle(), "Guru99 Bank Manager HomePage");
	  Assert.assertTrue(driver.findElement(By.xpath("//h2[text()='Guru99 Bank']")).isDisplayed());
	  
	  WebElement new_customer = driver.findElement(By.xpath("//a[text()='New Customer']"));
	  new_customer.click();
	  
	  //add new customer
	  sendKey(customername_txt,customer_name);
	  selectGender("f");
	  sendKey(dob_txt,dob);
	  sendKey(add_txt,add);
	  sendKey(city_txt,city);
	  sendKey(state_txt,state);
	  sendKey(pino_txt,pin);
	  sendKey(mobile_txt,mobile);
	  sendKey(email_txt,email);
	  sendKey(pass_txt,pass);
	  
	  click(submit_bt);
	  
	  // get customer id
	 String cusid_value = driver.findElement(customerId).getText(); 
	 System.out.println("Customer ID: " + driver.findElement(customerId).getText());
	 
	 //verify out data
	 Assert.assertEquals(driver.findElement(cusname_verify).getText(), customer_name);
	 Assert.assertEquals(driver.findElement(gender_verify).getText(), "female");
	 Assert.assertEquals(driver.findElement(dob_verify).getText(), dob);
	 Assert.assertEquals(driver.findElement(add_verify).getText(), add);
	 Assert.assertEquals(driver.findElement(city_verify).getText(), city);
	 Assert.assertEquals(driver.findElement(state_verify).getText(), state);
	 Assert.assertEquals(driver.findElement(pin_verify).getText(), pin);
	 Assert.assertEquals(driver.findElement(mobile_verify).getText(), mobile);
	 Assert.assertEquals(driver.findElement(email_verify).getText(), email);
	 
	  
	 
	 //choose edit customer, 
	 driver.findElement(By.xpath("//a[text()='Edit Customer']")).click();
	 sendKey(cusid_edit,cusid_value);
	 click(sub_edit);
	 
	 //verify customer name add
	 Assert.assertEquals(driver.findElement(customername_txt).getAttribute("value"), customer_name);
	 Assert.assertEquals(driver.findElement(add_txt).getText(), add);
	 
	 //input data in edit field
	 clearEditform();
	 sendkeyEditForm("243 Hoi Bun", "HK", "Tsim Sa Chuy", "147258", "0938456723",email);
	 click(submit_bt);
	 
	 //verify data afer edit
	 Assert.assertEquals(driver.findElement(add_verify).getText(), "243 Hoi Bun");
	 Assert.assertEquals(driver.findElement(city_verify).getText(), "HK");
	 Assert.assertEquals(driver.findElement(state_verify).getText(), "Tsim Sa Chuy");
	 Assert.assertEquals(driver.findElement(pin_verify).getText(), "147258");
	 Assert.assertEquals(driver.findElement(mobile_verify).getText(), "0938456723");
	 Assert.assertEquals(driver.findElement(email_verify).getText(), email);
	 
	  
  }
  
  
  
  @Test
  public void TC_2_checkTitle() {
	
	  
  }
  
  public int randomData() {
	  Random random = new Random();
	  return random.nextInt(999999);
	  
  }
  
  public void sendKey(By by, String value) {
	  driver.findElement(by).sendKeys(value);
	  
  }
  
  public void click(By by) {
	  driver.findElement(by).click();
  }
  
  public void selectGender(String value) {
	  if(value.equalsIgnoreCase("male")) {
		  driver.findElement(male_rb).click();
	  }else {
		  driver.findElement(female_rb).click();
	  }
  }
  
  public void clearEditform() {
	  driver.findElement(add_txt).clear();
	  driver.findElement(city_txt).clear();
	  driver.findElement(state_txt).clear();
	  driver.findElement(pino_txt).clear();
	  driver.findElement(mobile_txt).clear();
	  driver.findElement(email_txt).clear();   
  }
  
  public void sendkeyEditForm(String add_edit, String city_edit, String state_edit, String pino_edit, String moblie_edit, String email_edit) {
	  driver.findElement(add_txt).sendKeys(add_edit);
	  driver.findElement(city_txt).sendKeys(city_edit);
	  driver.findElement(state_txt).sendKeys(state_edit);
	  driver.findElement(pino_txt).sendKeys(pino_edit);
	  driver.findElement(mobile_txt).sendKeys(moblie_edit);
	  driver.findElement(email_txt).sendKeys(email_edit);
  }
  
  @AfterClass
  public void afterClass() {
	  driver.quit();
  }

}

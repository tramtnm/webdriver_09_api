package selenium;

import org.testng.annotations.Test;



import org.testng.annotations.BeforeClass;

import java.util.NoSuchElementException;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class Topic_02_Xpath_Css {
	
	WebDriver driver;

  @BeforeClass
  public void beforeClass() {
	  driver = new FirefoxDriver();
	  driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
	  driver.manage().window().maximize();
	  driver.get("http://live.guru99.com/");
  }
  
  public int randomNumber() {
		Random random = new Random();
		int number = random.nextInt(999999);
		System.out.println("Random number = " + number);
		return number;
}

  
  @Test
  public void TC_01_LoginWithEmailAndPasswordEmpty() {
	  driver.findElement(By.xpath("//div[@class='footer']//a[text()='My Account']")).click();
	  driver.findElement(By.id("email"));
	  driver.findElement(By.id("pass"));
	  driver.findElement(By.xpath("//button[@title='Login']")).click();
	  
	  String emailRequired = driver.findElement(By.id("advice-required-entry-email")).getText();
	  Assert.assertTrue(emailRequired.equals("This is a required field."));
	  
	  String passlRequired = driver.findElement(By.id("advice-required-entry-pass")).getText();
	  Assert.assertEquals(passlRequired, "This is a required field.");
  }
  
  @Test
  public void TC_02_LoginWithEmailInvalid() {
	  driver.findElement(By.xpath("//div[@class='footer']//a[text()='My Account']")).click();
	  driver.findElement(By.id("email")).sendKeys("123434234@12312.123123");
	  driver.findElement(By.xpath("//button[@title='Login']")).click();
	  
	  String emailInvalid = driver.findElement(By.id("advice-validate-email-email")).getText();
	  Assert.assertTrue(emailInvalid.equals("Please enter a valid email address. For example johndoe@domain.com."));
  }
  
  @Test
  public void TC_03_LoginWithPasswordLessThanSixChar() {
	  driver.findElement(By.xpath("//div[@class='footer']//a[text()='My Account']")).click();
	  driver.findElement(By.id("email")).sendKeys("automation@gmail.com");
	  driver.findElement(By.id("pass")).sendKeys("123");;
	  driver.findElement(By.xpath("//button[@title='Login']")).click();
	  
	  String passLess = driver.findElement(By.id("advice-validate-password-pass")).getText();
	  Assert.assertEquals(passLess, "Please enter 6 or more characters without leading or trailing spaces.");
  }
  
  @Test
  public void TC_04_LoginwithPasswordIncorrect() {
	  driver.findElement(By.xpath("//div[@class='footer']//a[text()='My Account']")).click();
	  driver.findElement(By.id("email")).sendKeys("automation@gmail.com");
	  driver.findElement(By.id("pass")).sendKeys("123123123");;
	  driver.findElement(By.xpath("//button[@title='Login']")).click();
	  
	  String passIncorrect = driver.findElement(By.xpath("//*[@id=\"top\"]/body/div/div/div[2]/div/div/div/ul/li/ul/li/span")).getText();
	  Assert.assertEquals(passIncorrect, "Invalid login or password.");
  }
  
  @Test
  public void TC_05_createAnAccount() {
	  driver.findElement(By.xpath("//div[@class='footer']//a[text()='My Account']")).click();
	  driver.findElement(By.xpath("//a[@title=\"Create an Account\"]")).click();
	  driver.findElement(By.xpath("//input[@id=\"firstname\"]")).sendKeys("Thuy Dung");
	  driver.findElement(By.xpath("//input[@id=\"lastname\"]")).sendKeys("Nguyen");
	  
	  String email = "automationtest" + randomNumber() + "@gmail.com";
	  driver.findElement(By.xpath("//input[@id=\"email_address\"]")).sendKeys(email);
	  driver.findElement(By.xpath("//input[@id=\"password\"]")).sendKeys("abc123456");
	  driver.findElement(By.xpath("//input[@id=\"confirmation\"]")).sendKeys("abc123456");
	  driver.findElement(By.xpath("//button[@title=\"Register\"]")).click();
	  
	  String conSignUp = driver.findElement(By.xpath("//*[@id=\"top\"]/body/div/div/div[2]/div/div[2]/div/div/ul/li/ul/li/span")).getText();
	  Assert.assertEquals(conSignUp, "Thank you for registering with Main Website Store.");
	  
	  driver.findElement(By.xpath("//div[@class=\"account-cart-wrapper\"]//a")).click();
	  driver.findElement(By.xpath("//a[text()=\"Log Out\"]")).click();
	  
	  String element = "//div[@class='footer']//a[text()='My Account']";
	  if(isElementDisplayed(driver, element)){
	  System.out.println("My account link is displayed");
	  } else{
	  System.out.println("My account link isn't displayed");
	  }
	  
  }
  
  private boolean isElementDisplayed(WebDriver driver, String yourLocator) {
		// TODO Auto-generated method stub
		  try {
			  WebElement locator;
			  locator = driver.findElement(By.xpath(yourLocator));
			  return locator.isDisplayed();
			  } catch (NoSuchElementException e) {
			  return false;
			  }
	}
  
  
  @AfterClass
  public void afterClass() {
	  driver.quit();
  }

}
